/* SPDX-License-Identifier: GPL-2.0 */

/*
 * Copyright (c) 2018 Datto, Inc.
 */

#ifndef _VOLSNAP_H
#define _VOLSNAP_H

/* Must be defined before including printk. */
#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <uapi/linux/volsnap.h>

int hook_syscall_table(void);
void restore_syscall_table(void);

#endif /* _VOLSNAP_H */
