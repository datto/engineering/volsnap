// SPDX-License-Identifier: GPL-2.0

/*
 * Copyright (c) 2018 Datto, Inc.
 */

#include "volsnap.h"

#include  <linux/fs.h>
#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/module.h>

static bool allow_mount_hook = true;
module_param(allow_mount_hook, bool, 0644);
MODULE_PARM_DESC(allow_mount_hook, "allow hooking syscall table for mount operations");

static DEFINE_MUTEX(volsnap_ioctl_mutex);

static long volsnap_ioctl(struct file *file, unsigned int cmd,
			  unsigned long param)
{
	int ret = -ENOTTY;

	mutex_lock(&volsnap_ioctl_mutex);
	switch (cmd) {
	}
	mutex_unlock(&volsnap_ioctl_mutex);

	return ret;
}

static const struct file_operations volsnap_ctl_fops = {
	.owner = THIS_MODULE,
	.llseek = noop_llseek,
	.unlocked_ioctl = volsnap_ioctl,
	.compat_ioctl = volsnap_ioctl,
	.open = nonseekable_open,
};

static struct miscdevice volsnap_misc = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = VOLSNAP_CONTROL_DEVICE,
	.fops = &volsnap_ctl_fops,
};

static int __init volsnap_init(void)
{
	int err;

	if (allow_mount_hook && hook_syscall_table() != 0)
		pr_warn("failed to locate syscall table");

	err = misc_register(&volsnap_misc);
	if (err < 0)
		return err;

	pr_info("module loaded\n");
	return 0;
}
module_init(volsnap_init);

static void __exit volsnap_exit(void)
{
	misc_deregister(&volsnap_misc);
	restore_syscall_table();
	pr_info("module unloaded\n");
}
module_exit(volsnap_exit);

MODULE_AUTHOR("Datto Inc. <agents@datto.com>");
MODULE_DESCRIPTION("Volume snapshots");
MODULE_LICENSE("GPL v2");
MODULE_VERSION("0.0.1");
