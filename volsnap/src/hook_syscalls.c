// SPDX-License-Identifier: GPL-2.0

/*
 * Copyright (c) 2018 Datto, Inc.
 */

#include "volsnap.h"
#include "symbols.h"

#include <linux/preempt.h>
#include <linux/unistd.h>

static void **syscall_table;

static asmlinkage long (*system_mount)(char __user *, char __user *,
	char __user *, unsigned long, void __user *);
static asmlinkage long (*system_umount)(char __user *, int);
#ifdef __NR_umount
static asmlinkage long (*system_oldumount)(char __user *);
#endif

static asmlinkage long volsnap_mount(char __user *dev_name,
	char __user *dir_name, char __user *type,
	unsigned long flags, void __user *data)
{
	return system_mount(dev_name, dir_name, type, flags, data);
}

static asmlinkage long volsnap_umount(char __user *name, int flags)
{
	return system_umount(name, flags);
}

#ifdef __NR_umount
static asmlinkage long volsnap_oldumount(char __user *name)
{
	return system_oldumount(name);
}
#endif

#define set_syscall(sys_nr, orig_call, new_call) \
	do { \
		orig_call = syscall_table[sys_nr]; \
		syscall_table[sys_nr] = new_call; \
	} while (0)

#define restore_syscall(sys_nr, orig_call) \
	do { \
		syscall_table[sys_nr] = orig_call; \
	} while (0)

static inline void disable_page_protection(void)
{
	write_cr0(read_cr0() & ~X86_CR0_WP);
}

static inline void enable_page_protection(void)
{
	write_cr0(read_cr0() | X86_CR0_WP);
}

static void **find_syscall_table(void)
{
	ptrdiff_t offset;
	void **sct;

	/*
	 * The address reported in the system map may not be the address used in
	 * the running kernel. Calculate the pointer offset between the two
	 * using a public symbol to get the correct address of the mount calls.
	 */
	offset = (void *)printk - PRINTK_PTR;
	sct = SYS_CALL_TABLE_PTR + offset;

	if (sct[__NR_mount] != SYS_MOUNT_PTR + offset ||
	    sct[__NR_umount2] != SYS_UMOUNT_PTR + offset)
		return NULL;

#ifdef __NR_umount
	if (sct[__NR_umount] != SYS_OLDUMOUNT_PTR + offset)
		return NULL;
#endif

	return sct;
}

/*
 * In order to ensure data consistency, we *must* be the last thing to access a
 * volume on unmount and the first to access it on mount. This requires
 * hooking into the syscall so we can add our logic around mount operations.
 */
int hook_syscall_table(void)
{
	syscall_table = find_syscall_table();
	if (!syscall_table)
		return -EFAULT;

	preempt_disable();
	disable_page_protection();
	set_syscall(__NR_mount, system_mount, volsnap_mount);
	set_syscall(__NR_umount2, system_umount, volsnap_umount);
#ifdef __NR_umount
	set_syscall(__NR_umount, system_oldumount, volsnap_oldumount);
#endif
	enable_page_protection();
	preempt_enable();

	pr_info("hooked syscall table at 0x%p\n", syscall_table);
	return 0;
}

void restore_syscall_table(void)
{
	if (!syscall_table)
		return;

	preempt_disable();
	disable_page_protection();
	restore_syscall(__NR_mount, system_mount);
	restore_syscall(__NR_umount2, system_umount);
#ifdef __NR_umount
	restore_syscall(__NR_umount, system_oldumount);
#endif
	enable_page_protection();
	preempt_enable();

	pr_info("syscall table restored\n");
}
