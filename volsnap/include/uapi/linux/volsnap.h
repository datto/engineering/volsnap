/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */

/*
 * Copyright (c) 2018 Datto, Inc.
 */

#ifndef _UAPI_VOLSNAP_H
#define _UAPI_VOLSNAP_H

#define VOLSNAP_CONTROL_DEVICE "volsnap-control"

#define VOLSNAP_IOCTL_MAGIC 0xC7

#endif /* _UAPI_VOLSNAP_H */
