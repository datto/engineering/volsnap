Volume Snapshots
================

**volsnap** is a Linux kernel module that creates a point-in-time snapshot of a
volume for the purpose of system backups.

## Licenses

This project makes use of [Software Package Data Exchange](https://spdx.org) to
identify licenses. Individual files contain SPDX identifiers instead of the full
license text.

- **volsnap** is made available under the terms of the GNU General Public
License, version 2.0.
- **libvolsnap** is made available under the terms of the GNU Lesser General
Public License, version 2.1 or later.
